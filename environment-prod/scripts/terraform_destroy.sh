# Chemins
root_dir="/builds/$GITLAB_USER_LOGIN/tp4_deploiement_continue"
env_dir="$root_dir/environment-prod"
terraform_dir="$env_dir/terraform"
ansible_dir="$env_dir/ansible"

cd $terraform_dir
terraform destroy -auto-approve
