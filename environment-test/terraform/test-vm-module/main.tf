# instance vm test
resource "google_compute_instance" "test_instance" {
  name                      = "test-instance"
  machine_type              = "g1-small"
  zone                      = "us-east1-b"
  tags                      = ["server-test"]
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }

  network_interface {
    network    = var.network_id_test
    subnetwork = var.subnetwork_id_test
    access_config {
      // necessary even empty
    }
  }

  metadata = {
    "ssh-keys" = "${var.user}:${file("~/.ssh/id_rsa.pub")}"
  }
}
