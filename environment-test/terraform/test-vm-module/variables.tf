variable "network_id_test" {
  type = string
}

variable "subnetwork_id_test" {
  type = string
}

variable "user" {
  default     = "constantin"
  description = "Nom de l'utilisateur"
  type        = string
}