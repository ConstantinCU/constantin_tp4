#!/bin/bash

# Chemins
root_dir="/builds/$GITLAB_USER_LOGIN/tp4_deploiement_continue"
env_dir="$root_dir/environment-test"
terraform_dir="$env_dir/terraform"
ansible_dir="$env_dir/ansible"

cd $terraform_dir
test_ip=$(terraform output test_instance_external_ip | sed 's/"//g')
user=$(terraform output instance_user | sed 's/"//g')

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 1/3: VERIFICATION DE LA PRESENCE DES FICHIERS ANSIBLE ------ #
echo -e "\033[1;35m- Etape 1/3: Vérification de la présence des fichiers Ansible\033[0m"

# Ajout clé publique pour l'adresse IP
ssh-keyscan $test_ip >> /root/.ssh/known_hosts

if [ ! -f "$ansible_dir/inventory.ini" ]; then
    echo -e "\033[33mFichier ansible inventory introuvable. Création du fichier 'inventory.ini'...\033[0m"

    # Génération de l'inventaire avec les adresses IP
    cd $env_dir/scripts
    ./generate_inventory.sh > $ansible_dir/inventory.ini
else
    echo -e "\033[32mFichiers Ansible déjà présents. Suppression et récréation de l'inventaire\033[0m"

    # Génération de l'inventaire avec les adresses IP
    cd $ansible_dir
    rm -f inventory.ini
    cd $env_dir/scripts
    ./generate_inventory.sh > $ansible_dir/inventory.ini
fi

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 2/3: LANCEMENT PLAYBOOKS ANSIBLE ------ #
echo -e "\033[1;35m- Etape 2/3: Lancement playbooks ansible\033[0m"

cd $ansible_dir
ansible-playbook -i inventory.ini -b test.yml

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 3/3: VERIFICATION FONCTIONNEMENT DES CONTAINERS SUR LE SERVEUR TEST ------ #
echo -e "\033[1;35m- Etape 3/3: Vérification fonctionnement des containers sur le serveur test\033[0m"

ssh -tt "$user@$test_ip" 'sudo docker ps'
ssh -tt "$user@$test_ip" 'sudo docker images'