#réseau
resource "google_compute_network" "network_dev" {
  name                    = "network-dev"
  auto_create_subnetworks = false
}

# sous-réseau pour dev
resource "google_compute_subnetwork" "subnetwork_dev" {
  name          = "subnetwork-dev"
  ip_cidr_range = "10.0.0.0/24"
  region        = "us-east1"
  network       = google_compute_network.network_dev.id
}

# firewall ssh
resource "google_compute_firewall" "firewall_ssh_dev" {
  name    = "allow-ssh-dev"
  network = google_compute_network.network_dev.self_link

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["server-dev"]
}

# firewall http https
resource "google_compute_firewall" "firewall_http_dev" {
  name    = "allow-http-https-dev"
  network = google_compute_network.network_dev.self_link

  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["server-dev"]
}
