variable "network_id_dev" {
  type = string
}

variable "subnetwork_id_dev" {
  type = string
}

variable "user" {
  default     = "constantin.formation2"
  description = "Nom de l'utilisateur"
  type        = string
}
