# MODULES
module "network" {
  source = "./dev-network-module"
}

module "dev_vm" {
  source        = "./dev-vm-module"
  network_id_dev    = module.network.network_id_dev
  subnetwork_id_dev = module.network.subnetwork_id_dev
}