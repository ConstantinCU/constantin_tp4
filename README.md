Ce dépôt contient une infrastructure de pipeline de déploiement continu pour les applications Python. 
Voici une description détaillée de chaque élément présent dans l'arborescence :

**Contenu de l'Arborescence**

_docker-compose.yml_ ->
Ce fichier contient la configuration nécessaire pour lancer l'environnement de développement.

_docker-scout.sh_ ->
Script utilisé pour vérifier la présence de vulnérabilités dans une image Docker. Il est appelé dans le pipeline pour la vérification de sécurité.

Dossier _environment-dev_ ->
Ce répertoire contient les scripts, les fichiers Terraform et Ansible pour créer et configurer l'environnement de développement.

Dossier _ansible_ -> Contient les configurations Ansible pour le déploiement.

Dossier _terraform_ -> Contient les fichiers Terraform pour la création des machines virtuelles nécessaires à l'environnement de développement.

Dossier _environment-test_ ->
Répertoire similaire à "environment-dev", mais dédié à la création et à la configuration de l'environnement de test.

Dossier _environment-prod_ ->
Répertoire similaire à "environment-dev" et "environment-test", mais dédié à la création et à la configuration de l'environnement de production.

_report-generator.sh_ ->
Script utilisé pour générer un rapport sur le déploiement et les tests effectués.

_run-functional-tests.sh_ ->
Script pour lancer les tests fonctionnels sur l'environnement cible.

_script1_deploy.sh_ ->
Script pour le déploiement de l'application selon l'environnement choisi.

_script2_register_runner.sh_ ->
Script pour enregistrer un runner ou une tâche spécifique dans le pipeline.

**Utilisation du Projet**



### Création du credentials.json ou clé de compte de service

Dans GCP - https://console.cloud.google.com/ - aller sur :

- 1 - IAM et administration
- 2 - Compte de service
- 3 - Créez un compte de service
- 4 - Entrez un nom puis continuez
- 5 - Séléctionnez le rôle propriétaire (ou éditeur) puis finalisez la création
- 6 - Cliquez sur le compte crée
- 7 - Cliquez sur clé en haut
- 8 - Cliquer sur ajouter une clé, en json
- 9 - Télécharger la clé en le renommant credentials.json
- 10 - Mettez le dans le dossier terraform/private (le dossier private n'existe pas créez le)
